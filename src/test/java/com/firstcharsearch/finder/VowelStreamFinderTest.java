package com.firstcharsearch.finder;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.firstcharsearch.stream.CharSequenceStream;
import com.firstcharsearch.stream.Stream;

/**
 * 
 * @author Felipe
 * @since 21/03/2018
 *  
 */
@RunWith(Parameterized.class)
public class VowelStreamFinderTest {
	
	private final Stream input;
    private final char expected;
    
    public VowelStreamFinderTest(Stream input, char expected) {
        this.input = input;
        this.expected = expected;
    }

	 /**
     * Collection with the test case and should return output     
     */
    @Parameterized.Parameters(name = "input:{0} - output:{1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
        	  {new CharSequenceStream("aAbBABacafe"), 'e'},
              {new CharSequenceStream("AAAAAaaaa"), ' '},
              {new CharSequenceStream("casa"), ' '},
              {new CharSequenceStream("acordar"), 'o'},
              {new CharSequenceStream(" "), ' '},
              {new CharSequenceStream("CAFE"), 'E'},
              {new CharSequenceStream("afe"), 'e'},
              {new CharSequenceStream("cafe"), 'e'},
              {new CharSequenceStream("aoBABacaee"), 'A'},
              {new CharSequenceStream("---[]afe"), 'e'},
              {new CharSequenceStream("�leo"), 'e'},
              {new CharSequenceStream("�gua"), 'u'},
              {new CharSequenceStream("�gUA"), 'U'},
              {new CharSequenceStream("�mago"), 'a'},
              {new CharSequenceStream("�mega"), 'e'},              
              {new CharSequenceStream("   abacate"), 'e'},
              {new CharSequenceStream("teleferico"), 'i'},
              {new CharSequenceStream("ttttttt"), ' '},
              {new CharSequenceStream("telefone"), 'o'},             
              {new CharSequenceStream("b"), ' '},
              {new CharSequenceStream("a"), ' '},
              {new CharSequenceStream("c--a--f--e"), ' '},
              {new CharSequenceStream("a[f--__e"), ' '},
              {new CharSequenceStream("voc�"), '�'}
        });
    }
    
    /**
     * Execution Test
     */
    @Test
    public void tryToFindVowelOnStream() {
        char actual = VowelStreamFinder.firstChar(input);
        assertThat(actual, is(equalTo(expected)));
    }
}