package com.firstcharsearch.stream;

import com.firstcharsearch.exception.InvalidStreamStateException;
import com.firstcharsearch.stream.CharSequenceStream;
import com.firstcharsearch.stream.Stream;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * 
 * @author Felipe
 * @since 21/03/2018
 *  
 */
public class CharSequenceStreamTest {

	 /**
     * should Process Each Character Of Stream
     * @param aAbBABacafe
     * @exception InvalidStreamStateException
     */
    @Test
    public void ProcessEachCharacterOfStream() {
        String source = "aAbBABacafe";
        char[] values = source.toCharArray();

        Stream stream = new CharSequenceStream(source);
        int i = 0;
        while (stream.hasNext()) {
            char actual = stream.getNext();
            assertThat(actual, is(equalTo(values[i++])));
        }
        assertThat(stream.hasNext(), is(equalTo(false)));
    }
    
    /**
     * NotifyNoMoreCharactersToProcess
     * @param abcdefghij
     * @exception InvalidStreamStateException
     */
    @Test
    public void NotifyMoreCharactersToProcess() {
        Stream stream = new CharSequenceStream("abcdef");
        assertThat(stream.hasNext(), is(equalTo(true)));
        assertThat(stream.getNext(), is(equalTo('a')));
    }

    /**
     * NotifyNoMoreCharactersToProcess
     * @param abcdefghij
     * @exception InvalidStreamStateException
     */
    @Test
    public void NotifyNoMoreCharactersToProcess() {
        Stream stream = new CharSequenceStream("abcdefghij");
        while (stream.hasNext()) {
            stream.getNext();
        }
        assertThat(stream.hasNext(), is(equalTo(false)));
    }
      
    /**
     * should Throw Exception When Invalid Stream
     * @param bbbS
     * @exception InvalidStreamStateException
     */
    @Test(expected = InvalidStreamStateException.class)
    public void InvalidStreamState() {
        Stream stream = new CharSequenceStream("bbbS");
        for (int i = 0; i < 5; i++) {
            stream.getNext();
        }
    }
    /**
     * should Throw Exception When Invalid Stream
     * @param null
     * @exception IllegalArgumentException
     */
    @Test(expected = IllegalArgumentException.class)
    public void InvalidStream() {
        new CharSequenceStream(null);
    }
}