package com.firstcharsearch.exception;

/**
 * 
 * @author Felipe
 * @since 21/03/2018
 *  
 */
public class InvalidStreamStateException extends RuntimeException {

    private static final long serialVersionUID = -6188074203275908046L;

    public InvalidStreamStateException() {
        super("No more characters to process");
    }
}
