package com.firstcharsearch.main;

import com.firstcharsearch.finder.VowelStreamFinder;
import com.firstcharsearch.stream.CharSequenceStream;

/**
 * 
 * @author Felipe
 * @since 21/03/2018
 *  
 */
public class MainApplication {

    public static void main(String[] args) {

    	String input = "aAbBABacafe";
        char foundChar = VowelStreamFinder.firstChar(new CharSequenceStream(input));
        if (foundChar != VowelStreamFinder.NOT_FOUND) {
            System.out.println("\nOutput: " + foundChar);
        } else {
            System.out.println("\nVowel character not localized. Please try another.");
        }
    }
}
