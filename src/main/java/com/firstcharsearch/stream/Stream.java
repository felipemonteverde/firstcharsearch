package com.firstcharsearch.stream;

/**
 * 
 * @author Felipe
 * @since 21/03/2018
 *  
 */
public interface Stream {

    public char getNext();
    public boolean hasNext();
}
