# Busca o primeiro caracter vogal apos uma consoante

## Enunciado

Dada uma stream, encontre o primeiro caractere Vogal, ap�s uma consoante, onde a mesma � antecessora a uma vogal e que n�o se repita no resto da stream. O termino da leitura da stream deve ser garantido atrav�s do m�todo hasNext(), ou seja, retorna falso para o termino da leitura da stream.

## Premissa

- Uma chamada para hasNext() ir retornar se a stream ainda contem caracteres para processar.
- Uma chamada para getNext() ir retornar o proximo caractere a ser processado na stream.
- N�o ser� poss�vel reiniciar o fluxo da leitura da stream.
- N�o poder� ser utilizado nenhum framework Java, apenas c�digo nativo.

## Exemplo: 
- Input:  aAbBABacafe
- Output: e
- No exemplo, �e� � o primeiro caractere Vogal da stream que n�o se repete ap�s a primeira Consoante �f�o qual tem uma vogal �a� como antecessora.


- Segue o exemplo da interface em Java:
public interface Stream{
    public char getNext();
    public boolean hasNext();
}
public static char firstChar(Stream input) {
}


